# MySQL 数据库表结构

# config 表
DROP TABLE IF EXISTS `crazy_config`;

CREATE TABLE `crazy_config` (
  `config_name` varchar(255) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  PRIMARY KEY (`config_name`)
) DEFAULT CHARSET=utf8;

# users 表
DROP TABLE IF EXISTS `crazy_users`;

CREATE TABLE IF NOT EXISTS `crazy_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sid` varchar(64) NOT NULL,
  `activation_key` varchar(64) NOT NULL DEFAULT '',
  `regtime` int(11) NOT NULL,
  `auth` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;