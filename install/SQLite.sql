# SQLite 数据库表结构

# config 表
DROP TABLE IF EXISTS crazy_config;

CREATE TABLE crazy_config (
  config_name varchar(255) NOT NULL DEFAULT '',
  config_value varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (config_name)
);

# users 表
DROP TABLE IF EXISTS crazy_users;

CREATE TABLE crazy_users (
  id INTEGER PRIMARY KEY NOT NULL,
  username varchar(32) NOT NULL DEFAULT '',
  password varchar(32) NOT NULL DEFAULT '',
  email varchar(255) NOT NULL DEFAULT '',
  sid varchar(64) NOT NULL DEFAULT '',
  activation_key varchar(64) NOT NULL DEFAULT '',
  regtime INTEGER(11) NOT NULL DEFAULT '0',
  auth tinyint(1) NOT NULL DEFAULT '0'
);